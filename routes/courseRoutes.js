const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");

// object destructuring of auth.js
// auth is an imported module, so therefore it is an object in JS
const { verify, verifyAdmin } = auth;


//Routes

//create/add course
router.post('/', verify, verifyAdmin, courseControllers.addCourse);

//get all courses
router.get('/', courseControllers.getAllCourses);

//get single course
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);

//archive
router.put('/archive/:id', verify, verifyAdmin, courseControllers.archive);

//activate
router.put('/activate/:id', verify, verifyAdmin, courseControllers.activate);

//get active courses
router.get('/getActiveCourses', courseControllers.getActiveCourses);

// update a course
router.put("/:id", verify, verifyAdmin, courseControllers.updateCourse);

// get inactive course
router.get("/getInactiveCourses", verify, verifyAdmin, courseControllers.getInactiveCourses);

// find courses by name
router.post("/findCoursesByName", courseControllers.findCoursesByName);

// find course by price
router.post("/findCoursesByPrice", courseControllers.findCoursesByPrice)

// get a course's enrollments
router.get("/getEnrollees/:id", verify, verifyAdmin, courseControllers.getEnrollees);

module.exports = router;