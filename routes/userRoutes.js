const express = require('express');
const router = express.Router();

// import user controllers
const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const { verify, verifyAdmin } = auth;

// Routes

// User Registration
router.post("/", userControllers.registerUser);

// Retrieve All Users
router.get("/", userControllers.getAllUsers);

// Login
router.post("/login", userControllers.loginUser);

// Retrieve User Details
router.get("/getUserDetails", verify, userControllers.getUserDetails)

// Check if Email Exists
router.post('/checkEmailExists', userControllers.checkEmailExists);


/*
	Mini Activity

	>> Create a user route which is able to capture an id from its url using route params.
		>> this route will be used to update a regular user to an admin
		>> only an admin can access this route
		>> name your controller: updateAdmin
		>> name your endpoint: "/updateAdmin/___"

*/
// Updating a regular user to admin
router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin);

// Update user details
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

// Enroll registered user
router.post("/enroll", verify, userControllers.enroll);

// Get the enrollments of a logged in user
router.get("/getEnrollments", verify, userControllers.getEnrollments);

module.exports = router;