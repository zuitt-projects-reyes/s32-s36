//import Course Model
const Course = require("../models/Course");

// add course
module.exports.addCourse = (req, res) => {

	console.log(req.body);

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})

	newCourse.save()
		.then(course => res.send(course))
		.catch(error => res.send(error))


}

// get all course
module.exports.getAllCourses = (req, res) => {

	//find all documents in the Course collection
	//We will use the find() method of our Course model
	//Because the Course model is connected to our courses collection

	Course.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error));

}

//get single course
module.exports.getSingleCourse = (req, res) => {

	//console.log(req.params.id)//Check the if you can receive the data coming from the url.

	//let id = req.params.id

	Course.findById(req.params.id)
		.then(result => res.send(result))
		.catch(err => res.send(err))

}

// archiving course
module.exports.archive = (req, res) => {

	//console.log(req.params.id);//course's id

	//updates object will contain the field/fields to update and its new value.
	let updates = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id, updates, { new: true })
		.then(updatedCourse => res.send(updatedCourse))
		.catch(err => res.send(err));

}

// activating course
module.exports.activate = (req, res) => {

	//console.log(req.params.id);//course's id

	//updates object will contain the field/fields to update and its new value.
	let updates = {
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id, updates, { new: true })
		.then(updatedCourse => res.send(updatedCourse))
		.catch(err => res.send(err));

}

// get all active courses
module.exports.getActiveCourses = (req, res) => {

	Course.find({ isActive: true })
		.then(result => res.send(result))
		.catch(err => res.send(err));

}

// updating course

module.exports.updateCourse = (req, res) => {

	console.log(req.params.id);
	console.log(req.body);

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, updates, { new: true })
		.then(updatedCourse => res.send(updatedCourse))
		.catch(err => res.send(err));
};

// get inactive courses

module.exports.getInactiveCourses = (req, res) => {

	Course.find({ isActive: false })
		.then(result => res.send(result))
		.catch(err => res.send(err));
};

// find courses by name

module.exports.findCoursesByName = (req, res) => {

	Course.find({ name: { $regex: req.body.name, $options: '$i' } })
		.then(result => {

			if (result.length === 0) {
				return res.send("No courses found");
			} else {
				return res.send(result);
			}
		})
		.catch(err => res.send(err));
};

// find courses by price

module.exports.findCoursesByPrice = (req, res) => {

	Course.find({ price: req.body.price })
		.then(result => {

			console.log(result)
			if (result.length === 0) {
				return res.send("No course found");

			} else {
				return res.send(result);
			}
		})
		.catch(err => res.send(err));
};

// get a course's enrollees and send back the result to the user

module.exports.getEnrollees = (req, res) => {

	Course.findById(req.params.id)
		.then(course => {

			if (course.enrollees.length === 0) {
				return res.send("No enrollees found");
			} else {
				return res.send(course.enrollees);
			}
		})
		.catch(err => res.send(err));
};
